package com.affymetrix.igb.util;

import com.affymetrix.igb.swing.JRPMenu;

/**
 *
 * @author dcnorris
 */
public interface MainMenuManager {

    JRPMenu getMenu(String menuId);

}
